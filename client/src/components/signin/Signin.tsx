import React from 'react';
import { Grid, Input, TableRow, TableCell, Button } from '@material-ui/core'
import { Store } from '../../store';

export function Signin() {
    const [username, setUsername] = React.useState('');
    const { dispatch } = React.useContext(Store);

    const onSubmit = (e: React.FormEvent) => {
        e.preventDefault();

        if (username === '') {
            dispatch({
                type: 'AUTH_ERROR',
                payload: {
                    error: 'Username cannot be empty!',
                },
            });
        }

        dispatch({
            type: 'LOGIN',
            payload: {
                username,
            },
        });
    };

    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            style={{ minHeight: '100vh' }}
        >
            <form onSubmit={onSubmit}>
                <h5
                margin-bottom="1rem"
                >What's your name?</h5>
                <Input
                    autoComplete="off"
                    name="username"
                    value={username}
                    onChange={e => setUsername(e.target.value)}
                />
                <TableRow>
                    <TableCell align="center" style={{borderBottom: "none"}}>
                        <Button color="primary" type="submit">
                            Start Chatting button
                        </Button>
                    </TableCell>
                </TableRow>
            </form>
        </Grid>
    );
}
