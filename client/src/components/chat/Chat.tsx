import React from 'react';
import { Button, Card, Input, InputAdornment } from '@material-ui/core';
import styled from 'styled-components';
import { socket } from '../../socket';
import { Store } from '../../store';
import { Message } from '../../store/interfaces';
import Container from "@material-ui/core/Container";

export function Chat() {
    const [msg, setMessage] = React.useState('');
    const { state, dispatch } = React.useContext(Store);
    const { messages } = state.message;
    const { users, username } = state.auth;

    React.useEffect(() => {
        // Listen fo message events and update state by calling dispatch
        socket.on('message', (message: Message) => {
            dispatch({
                type: 'GET_MESSAGE',
                payload: {
                    message,
                },
            });
        });
    }, []);

    // Validate message and dispatch corresponding actions
    const sendMessage = (e: React.FormEvent) => {
        e.preventDefault();

        if (msg === '') {
            return dispatch({
                type: 'SEND_MESSAGE_ERROR',
                payload: {
                    error: 'Cannot send an empty message!',
                },
            });
        }

        const message = {
            sender: username,
            message: msg,
        };

        dispatch({
            type: 'SEND_MESSAGE',
            payload: {
                message,
            },
        });

        setMessage('');
    };

    // Trigger when pressing the log out button on top left corner
    const logout = (e: React.MouseEvent) => {
        e.preventDefault();
        dispatch({
            type: 'LOGOUT',
            payload: {
                username,
            },
        });
    };

    // Remove current user from users array
    // Render users from the new array
    const renderUsers = (users: string[]) => {
        const index = users.indexOf(username);
        let newUsers = users;

        if (index > -1) {
            newUsers.splice(index, 1);
        }

        return newUsers && newUsers.length !== 0 ? (
            newUsers.map((user: string, index: number) => (
                <React.Fragment key={index}>
                    <h2 style={{ color: '#42f44b' }}>Online:</h2>
                    <ul>
                        <li>
                            <p>{user}</p>
                        </li>
                    </ul>
                </React.Fragment>
            ))
        ) : (
            <h2 style={{ color: '#f4416b' }}>Nobody online</h2>
        );
    };

    const renderMessages = (messages: Message[]) =>
        messages.map((message: Message, index: number) => (
            <ul key={index}>
                <li>
          <span style={{fontSize: "20px"}}>
            {message.sender}: <MessageCard>{message.message}</MessageCard>
          </span>
                </li>
            </ul>
        ));


    return (
        <ChatCard>
            <Sidebar>
        <span>
          <h2>{username}</h2>
        </span>
                <Button
                    onClick={logout}
                    color="primary"
                    type="submit"
                >
                    log out
                </Button>
                <UserList>{users && renderUsers(users)}</UserList>
            </Sidebar>
            <Main>
                <MessageList>
                    {messages ? (
                        renderMessages(messages)
                    ) : (
                        <h1>No messages to show</h1>
                    )}
                </MessageList>
                <ChatCardForm onSubmit={sendMessage}>
                    <Input
                        type="text"
                        placeholder="Type a message..."
                        value={msg}
                        onChange={e => setMessage(e.target.value)}
                        fullWidth={true}
                        endAdornment={<InputAdornment position="end">
                            <Button
                                color="primary"
                                type="submit"
                                style={{ borderRadius: '0 0.25rem 0.25rem 0' }}
                            >
                                Send
                            </Button>
                        </InputAdornment>}
                    />
                </ChatCardForm>
            </Main>
        </ChatCard>
    );
}

const ChatCard = styled(Card)`
  height: 100%;
  border-radius: 0.5rem;
  border: 0.1rem solid var(--light);
  background: var(--dark-opacity);
  display: flex;
  flex-direction: row;
  fontSize: 20px;
  h1 {
    font-size: 1.5rem;
  }

  h2 {
    font-size: 20px;
  }
`;

const Sidebar = styled.div`
  width: 10rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: var(--light-opacity);
  border-radius: inherit;
  padding: 0.5rem;
  margin: 0.5rem;

  button {
    width: 5rem;
    border: 0.1rem solid var(--light);
    margin-bottom: 1rem;

    &:hover {
      background: var(--light);
      border: 0.1rem solid var(--danger);
      color: var(--danger);
    }
  }

  span {
    margin-bottom: 1rem;
  }

  @media only screen and (max-width: 600px) {
    display: none;
  }
`;

const Main = styled(Container)`
  display: flex;
  flex-direction: column;
  border-radius: inherit;
`;

const UserList = styled.div`
  overflow-y: auto;
  height: 100%;
  margin-right: 0.5rem;
  padding-right: 2.5rem;

  h2 {
    margin-right: -2.5rem;
  }

  ul {
    list-style: none;
  }
`;

const MessageList = styled(Container)`
  border-radius: inherit;
  padding: 0.5rem;
  margin: 0.5rem 0 0.5rem 0;
  text-align: left;
  overflow-y: auto;
  height: 100%;

  h1 {
    text-align: center;
  }

  ul {
    list-style: none;

    li {
      margin-left: -2rem;

      span {
        display: flex;
      }
    }
  }
`;

const MessageCard = styled.div`
  background: var(--msg-card-bg);
  max-width: 90rem;
  min-width: 2rem;
  margin-left: 0.5rem;
  padding: 0.25rem;
  padding-top: 0;
  border-radius: 0.5rem;
`;

const ChatCardForm = styled.form`
  margin-bottom: 0.5rem;

  input {
    border-radius: 0.25rem;
  }
`;
