import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import './index.css';
import { Routes } from './Routes';
import { socket } from './socket';
import { Store, StoreProvider } from './store';

const App = () => {
    const { dispatch } = React.useContext(Store);

    socket.on('connect', () => console.log('connected'));

    React.useEffect(() => {
        dispatch({
            type: 'GET_USER',
            payload: {},
        });

        socket.on('users', (users: string[]) => {
            dispatch({
                type: 'GET_USERS',
                payload: {
                    users,
                },
            });
        });
    }, []);

    return (
        <Container>
            <Routes />
        </Container>
    )
};

const Container = styled.div`
  height: 100vh;
  padding: 0.75rem;
  text-align: center;
  color: var(--semi-dark-opacity);
`;

ReactDOM.render(
    <StoreProvider>
        <App />
    </StoreProvider>,
    document.getElementById('root'),
);
