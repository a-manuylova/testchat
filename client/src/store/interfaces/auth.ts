export interface IAuthAction {
  type: string;
  payload: {
    username?: string;
    error?: string;
    users: string[];
  };
}

export interface IAuthState {
  username: string | null;
  isAuthenticated: boolean | null;
  users: string[] | null;
  error: string | null;
}
