import { IAuthState } from './auth';
import { IMessageState } from './message';

export interface IState {
  auth: IAuthState;
  message: IMessageState;
}
