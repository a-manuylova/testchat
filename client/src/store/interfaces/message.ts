export type Message = {
  sender: string;
  message: string;
};

export interface IMessageAction {
  type: string;
  payload: {
    message: Message;
    error?: string;
  };
}

export interface IMessageState {
  messages: Message[] | null;
  error: string | null;
}
