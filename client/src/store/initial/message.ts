import { IMessageState } from '../interfaces';

export const initialMessageState: IMessageState = {
  messages: null,
  error: null,
};
