import { IAuthState } from '../interfaces';

export const initialAuthState: IAuthState = {
  username: null,
  isAuthenticated: null,
  users: null,
  error: null,
};
