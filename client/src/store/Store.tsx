import React from 'react';
import { initialAuthState, initialMessageState } from './initial';
import { IState } from './interfaces';
import { authReducer, combineReducers, messageReducer } from './reducers';

export const initialState = {
  auth: initialAuthState,
  message: initialMessageState,
};

export const Store = React.createContext<IState | any>(initialState);

const reducers = {
  auth: authReducer,
  message: messageReducer,
};

const reducer = combineReducers(reducers);

export function StoreProvider({ children }: any) {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  return (
    <Store.Provider value={{ state, dispatch }}>{children}</Store.Provider>
  );
}
