import { socket } from '../../socket';
import { initialMessageState } from '../initial';
import { IMessageAction } from '../interfaces';

export const messageReducer = (
  state = initialMessageState,
  action: IMessageAction,
) => {
  const { message, error } = action.payload;
  const { messages } = state;

  switch (action.type) {
    case 'GET_MESSAGE':
      return {
        ...state,
        messages: messages ? [...messages, message] : [message],
      };

    case 'SEND_MESSAGE':
      socket.emit('message', message);

    case 'SEND_MESSAGE_ERROR':
      console.log(error);

    case 'TYPE':
      socket.emit('typing', message);

    default:
      return state;
  }
};
