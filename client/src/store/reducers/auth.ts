import { socket } from '../../socket';
import { initialAuthState } from '../initial';
import { IAuthAction } from '../interfaces';

export const authReducer = (state = initialAuthState, action: IAuthAction) => {
  const { username, error, users } = action.payload;
  const existingUser = window.localStorage.getItem('username');

  switch (action.type) {
    case 'LOGIN':
      username && window.localStorage.setItem('username', username);
      socket.emit('login', username);

      return {
        ...state,
        isAuthenticated: true,
        username,
      };

    case 'GET_USER':
      if (existingUser) {
        socket.emit('login', existingUser);

        return {
          ...state,
          isAuthenticated: true,
          username: existingUser,
        };
      }

    case 'LOGOUT':
      window.localStorage.removeItem('username');
      username && socket.emit('logout', username);

      return {
        ...state,
        isAuthenticated: false,
        username: null,
      };

    case 'AUTH_ERROR':
      return {
        ...state,
        error,
      };

    case 'GET_USERS':
      return {
        ...state,
        users,
      };

    default:
      return state;
  }
};
