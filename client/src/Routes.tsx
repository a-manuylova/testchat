import React from 'react';
import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch,
} from 'react-router-dom';
import { Chat, Signin } from './components';
import { Store } from './store';

export const Routes = () => {
    const store = React.useContext(Store);

    return (
        <Router>
            <Switch>
                <Redirect exact from="/" to="/signin" />
                {store.state.auth.isAuthenticated ? (
                    <Redirect from="/signin" to="/chat" />
                ) : (
                    <Redirect from="/chat" to="/signin" />
                )}
                <Route path="/signin" component={ Signin } />
                <Route path="/chat" component={ Chat } />
            </Switch>
        </Router>
    )
}
