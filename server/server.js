require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http').Server(app);
const cors = require('cors');
const io = require('socket.io')(http);

app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:3000', credentials: true }));

const PORT = process.env.PORT || 3003;
http.listen(PORT, () => console.log(`Server listening at port ${PORT}`));

// Remove user from array of clients
const removeFromChat = (username) => {
    if (username) {
        const i = clients.indexOf(username);
        clients.splice(i, 1);
    }
};

// Store clients in an array
let clients = [];

// Establish connection
io.on('connection', (socket) => {
    // Listen for login
    socket.on('login', (username) => {
        console.log(username, 'logged in');

        // Set username attribute on socketProvider
        socket.username = username;

        clients.push(username);

        // Send updated clients array to all clients
        io.emit('users', clients);
    });

    socket.on('logout', (username) => {
        console.log(username, 'logged out');

        // Remove user from clients
        removeFromChat(username);

        // Send updated clients array to all clients
        io.emit('users', clients);
    });

    // Listen for message events
    socket.on('message', (message) => io.emit('message', message));

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', () => {
        console.log('sd');
        const { username } = socket;
        socket.broadcast.emit('typing', { username });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', () => {
        const { username } = socket;
        socket.broadcast.emit('stop typing', { username });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', () => {
        const { username } = socket;
        username && console.log(username, 'disconnected');

        // Remove user from clients
        removeFromChat(username);

        // Send updated clients array to all clients
        io.emit('users', clients);
    });
});
